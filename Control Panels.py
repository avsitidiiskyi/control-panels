from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from time import sleep

path_to_driver = r'D:\Python\WEBDRIVER\chromedriver.exe'

SUBMIT = "//button[@type='submit']"
LOGIN = "//input[@type='text']"
PASSWORD = "//input[@type='password']"

while True:
    try:
        control_panel_type = int(input("1 - SAM\n2 - SODA\n3 - EXPERT\n4 - ARCHITECT\n"
                                       "Choose CP that you need: "))
        if control_panel_type in range(1, 5):
            break
    except ValueError:
        print("Enter proper int value in range 1-4")
control_panel_type -= 1

while True:
    try:
        environment = int(input("1 - STAGE\n2 - PROD\n"
                                "Now, choose the environment you need: "))
        if environment == 1 or environment == 2:
            break
    except ValueError:
        print("Enter proper int value in range 1-2")
environment -= 1
environments = ['stage-', '']

control_panels = [f"https://{environments[environment]}[under NDA]",
                  f"https://{environments[environment]}[under NDA]",
                  f"https://{environments[environment]}[under NDA]",
                  f"https://{environments[environment]}[under NDA]"]

driver = webdriver.Chrome(executable_path=path_to_driver)
driver.get(control_panels[control_panel_type])
driver.maximize_window()
try:
    iter_element = WebDriverWait(driver, 60).until(
        ec.presence_of_element_located((By.XPATH, SUBMIT))
    )
except Exception:
    driver.quit()

sleep(1)
driver.find_element_by_xpath(LOGIN).send_keys("required_login")
sleep(0.5)
driver.find_element_by_xpath(PASSWORD).send_keys("required_password")
sleep(0.5)
driver.find_element_by_xpath(SUBMIT).click()
"""
Build compilation script: 
1)cd existing folder;
2)pyinstaller --add-data "file_name+extension;." -F -i "path to icon.ico" file_name.py

Libraries: Selenium, Pyinstaller.
"""
